module.exports = {
    "env": {
        "es6": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 2018
    },
	"globals": {
		"_bot": "readonly",
		"_database": "readonly",
		"_util": "readonly",
		"db": "readonly",
		"conn": "readonly",
		"constants": "readonly"
	},
    "rules": {
        "indent": [
            "error",
            "tab"
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ],
		"no-console": "off"
    }
};
