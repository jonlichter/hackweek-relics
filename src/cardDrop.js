let similarity = require('string-similarity');
let _ = require('lodash');
let nanoid = require('nanoid');

let distortEngine = require('./distort/engine.js');
let cards = require('./cards');

let revealTime = 60000;

module.exports = {
	_drops: {},
	_dropIds: {},
	_servers: {},

	spawn: async function (channel) {
		let guildSettings = await _database.fetchGuild(channel.guild.id);
		if (!guildSettings.flags.itemdrop) {
			return;
		}

		let card = this.chooseCard();
		this._drops[channel.id] = card;

		let dropId = nanoid();
		this._dropIds[channel.id] = dropId;

		let imageId = await distortEngine.distort(card.asset, 3);

		try {
			let m = await channel.send({
				embed: {
					author: {
						name: _bot.bot.user.username,
						icon_url: _bot.bot.user.avatarURL
					},
					color: constants.EMBED_INFO,
					title: 'Item Drop!',
					description: 'Use `r.claim <your guess>` to claim it.',
					timestamp: new Date(),
					footer: {
						icon_url: _bot.bot.user.avatarURL,
						text:'Discord Relics - A Hackweek Project'
					}
				},
				files: [
					'./distorted/' + imageId + '.png'
				]
			});
			setTimeout(function () {
				// if there's a new drop
				if (this._dropIds[channel.id] != dropId) {
					return;
				}

				let embed = {
					embed: {
						author: {
							name: _bot.bot.user.username,
							icon_url: _bot.bot.user.avatarURL
						},
						color: constants.EMBED_NEGATIVE,
						title: 'Item Drop!',
						description: 'Nobody guessed the item in time. It was **' + card.name + '**.',
						timestamp: new Date(),
						footer: {
							icon_url: _bot.bot.user.avatarURL,
							text:'Discord Relics - A Hackweek Project'
						}
					}, // i dont think it is possible to edit attachments
					files: [
						'./assets/' + card.asset + '.png'
					]
				};
				m.edit(embed);
				channel.send(embed);

				// timeout
				this.claimDrop(channel);
			}.bind(this), revealTime);
		} catch (e) {
			// channel.send('Failed to drop items! I\'m probably missing permissions to upload images.');
		}
	},

	chooseCard: function () {
		return _.sample(cards.bySet.random);
	},

	checkClaim: function (channel, guess) {
		let drop = this._drops[channel.id];

		if (!drop) {
			return {
				success: -1
			};
		}

		let sim = similarity.compareTwoStrings(drop.name.toLowerCase(), guess.toLowerCase());
		console.log('similarity report: "' + drop.name + '", "' + guess +'", ' + sim);

		if (sim > 0.8) {
			return {
				success: 1,
				cardId: drop.id,
				asset: drop.asset
			};
		} else {
			return {
				success: 0,
				cardId: drop.id,
				asset: drop.asset
			};
		}
	},

	claimDrop: function (channel) {
		this._drops[channel.id] = null;
		this._dropIds[channel.id] = null;
	}
};
