let Discord = require('discord.js');
let DBL = require('dblapi.js');
let _ = require('lodash');

let config = require('./config');
let cardDrop = require('./cardDrop');

let channelActivity = {};

let presences = ['all the items! | r.intro', 'Hackweek 2019 | r.intro', 'the cards... | r.intro'];
let presenceIndex = _.random(0, presences.length - 1);

module.exports = {
	init: function () {
		let bot = new Discord.Client({});
		this.bot = bot;

		// dbl server count
		if (config.dbl_token) {
			let dbl = new DBL(config.dbl_token, bot);
			dbl.on('error', function () {
				console.log('[SHARD ' + bot.shard.id + '] DBL server count posted');
			});
			dbl.on('error', function (e) {
				console.log('[SHARD ' + bot.shard.id + '] DBL error: ' + e);
			});
		}

		let prefixes = ['relic ', 'r\\.'];

		// simple error reporting
		bot.on('error', (e) => console.error(e));
		bot.on('warn', (e) => console.warn(e));
		// bot.on('debug', (e) => console.info(e));

		bot.on('ready', function () {
			prefixes.push('<@!?' + bot.user.id + '> ');

			setInterval(function () {
				this.nextPresence();
			}.bind(this), 60000);
			this.nextPresence();

			console.log('[SHARD ' + bot.shard.id + '] Discord client ready');
		}.bind(this));

		bot.on('message', async function (message) {
			if (message.author.bot) {
				return;
			}

			let regex = new RegExp('^(' + prefixes.join('|') + ')');
			let prefixed = message.content.match(regex);
			if(!prefixed) {
				this.onOtherMessage(message);
				return;
			}

			let args = message.content.slice(prefixed[0].length).split(/\s+/g);
			let command = args.shift().toLowerCase();

			let commandFile;

			try {
				commandFile = require('./commands/' + command);
			} catch (e) {
				let alias = config.alias[command];

				if (alias) {
					try {
						commandFile = require('./commands/' + alias);
					} catch (e) {
						console.log(e);
					}
				} else {
					console.log(e);
				}
			}

			if (commandFile) {
				commandFile.execute(args, message);
			}
		}.bind(this));

		bot.login(config.token);
	},

	nextPresence: function () {
		presenceIndex += 1;
		if (presenceIndex > presences.length - 1) {
			presenceIndex = 0;
		}
		this.bot.user.setPresence({ game: { name: presences[presenceIndex], type: 'WATCHING' } });
	},

	// TODO antispam (levenstein distance, different users, time)
	onOtherMessage: function (message) {
		if (!channelActivity[message.channel.id]) {
			channelActivity[message.channel.id] = {
				progress: 0
			};
		}

		channelActivity[message.channel.id].progress += message.content.length;

		if (channelActivity[message.channel.id].progress >= 1500) {
			cardDrop.spawn(message.channel);
			channelActivity[message.channel.id] -= 1500;
		}
	}
};
