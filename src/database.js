let r = require('rethinkdb');

let config = require('./config');

let defaultDatabase = config.rethinkdb_db;

module.exports = {
	init: async function () {
		await this.connect();

		await this.ensureDatabase(defaultDatabase);
		await this.ensureTable('users', { primaryKey: 'id' });
		await this.ensureTable('guilds', { primaryKey: 'id' });
	},

	connect: function () {
		return new Promise(function(resolve, reject) {
			r.connect({
				host: config.rethinkdb_ip,
				port: 28015
			}, function (err, conn) {
				if (err) {
					reject(err);
					return;
				}

				conn.use(defaultDatabase);
				this.connection = conn;

				global.db = r;
				global.conn = conn;

				console.log('[SHARD ' + _bot.bot.shard.id + '] Database connected');

				resolve(conn);
			}.bind(this));
		}.bind(this));
	},

	fetchUser: async function (id) {
		let record = await db.table('users').get(id).run(conn);

		if (!record) {
			let result = await db.table('users').insert({
				id: id,
				money: 100,
				inventory: {},
				packs: {},
				promos: {},
				stats: {}
			}, { returnChanges: true }).run(conn);
			record = result.changes[0].new_val;
		}

		return record;
	},

	fetchGuild: async function (id) {
		let record = await db.table('guilds').get(id).run(conn);

		if (!record) {
			let result = await db.table('guilds').insert({
				id: id,
				flags: {
					itemdrop: true
				}
			}, { returnChanges: true }).run(conn);
			record = result.changes[0].new_val;
		}

		return record;
	},

	ensureDatabase: async function (name) {
		if (!await r.dbList().contains(name).run(this.connection)) {
			await r.dbCreate(name).run(this.connection);
			console.log('[SHARD ' + _bot.bot.shard.id + '] Recreated database', name);
		}
	},

	ensureTable: async function (name, options) {
		if (!await r.tableList().contains(name).run(this.connection)) {
			await r.tableCreate(name, options).run(this.connection);
			console.log('[SHARD ' + _bot.bot.shard.id + '] Recreated table ' + defaultDatabase + '.' + name);
		}
	}
};
