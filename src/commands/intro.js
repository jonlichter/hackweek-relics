let pages = [
	'Hello adventurer! Welcome to **Discord Relics**, a trading card game. In Discord Relics you get to collect items, open packs, and complete your collection. We\'re so glad you decided to join us in our bot journey... Let\'s get started!\n\n*Type `r.intro 2` to continue*',
	'The currency for Discord Relics is :dollar:. You can earn :dollar: by claiming your `r.daily` and playing card guess! :dollar: can be used in the shop. You can see the shop\'s offers with `r.shop view` and you can buy an offer with `r.shop buy <the name of the offer>`.',
	'Packs are the main way to get cards. You can see your packs with `r.pack list`. You can also open packs with `r.pack open <the name of the pack>`. After you open some packs, your items can be seen with `r.inventory`. `r.completion` will show you how close you are to completing different sets of items. Can you collect them all?',
	'The bot will automatically spawn cards into chatrooms depending on how active the chat is. You can then try to guess what the card is. Beware! The cards are difficult to guess because of distortion and other wonky image effects! If you can guess the card, you get to keep it!',
	'You probably won\'t know any of the cards when you start but as you keep playing and learning from your mistakes, you will be able to identify them all! You can look at any item with `r.item <the name of the item>`. This is a good way to learn more of the cards.',
	'In addition to shops and packs, there\'s two more fun things you can do with the bot! The first is `r.cardguess`, or `r.cg`. You can try to guess the cards... but at a cost! If you get them right, you will be rewarded handsomely. Try to learn the cards and test your knowledge by playing cardguess!',
	'We will continue updating the bot after Discord Hack Week 2019 is over with new features and content. Feeling generous? Vote for us on Discord Bot List using `r.vote`. If you get stuck, check the help or join the support server (using `r.info`). Thanks for following the entire tutorial, use promo code `thanks-for-reading` for a little starting gift :wink:. We hope you enjoy Discord Relics!'
];

module.exports = {
	name: 'intro',
	documentation: {
		name: 'intro',
		description: 'Read a short tutorial on how to use Discord Relics.',
		usage: '[page = 1]'
	},
	execute: async function (args, message) {
		let client = _bot.bot;

		let page = parseInt(args[0]) || 1;

		message.channel.send({
			embed: {
				author: {
					name: client.user.username,
					icon_url: client.user.avatarURL
				},
				color: constants.EMBED_INFO,
				title: 'Introduction [' + page + ' of ' + pages.length + ']',
				description: pages[page - 1] || 'Unknown page.',
				timestamp: new Date(),
				footer: {
					icon_url: client.user.avatarURL,
					text:'Discord Relics - A Hackweek Project'
				}
			}
		});
	}
};
