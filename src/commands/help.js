let _ = require('lodash');
let fs = require('fs');

let chunkSize = 20;

// load commands from files
let commands = {};
let files = fs.readdirSync('./src/commands/');
files.forEach(function (filename) {
	if (filename == 'help.js') {
		// can't recursively load
		return;
	}

	let command = require('./' + filename);
	if (!command.documentation.hidden) {
		commands[command.name] = command.documentation;
	}
});

module.exports = {
	name: 'help',
	documentation: {
		name: 'help',
		description: 'See help for commands',
		usage: 'help | help <command>',
		hidden: true
	},
	execute: async function (args, message) {
		if (args.length == 0) {
			let out = [];
			Object.keys(commands).forEach(function (k) {
				let v = commands[k];

				out.push('**' + v.name + '** ' + (v.usage ? '(`' + v.usage + '`)' : '') + ': ' + v.description);
			});

			let chunked = _.chunk(out, chunkSize);

			// send all the commands, in chunks of COMMANDS_PER_MESSAGE commands each.
			for (let i = 0; i < chunked.length; i++) {
				await message.channel.send({embed: {
					color: constants.EMBED_INFO,
					author: {
						name: _bot.bot.user.username,
						icon_url: _bot.bot.user.avatarURL
					},
					title: 'Discord Relics Commands [' + (i + 1) + ' of ' + chunked.length + ']',
					description: chunked[i].join('\n'),
					timestamp: new Date(),
					footer: {
						icon_url: _bot.bot.user.avatarURL,
						text: 'Discord Relics - A Hackweek Bot'
					}
				}});
			}
		} else {
			// get the command info
			let helpData = commands[args.join(' ')];

			// if there's no command found, return with a message
			if (!helpData) {
				message.channel.send('Couldn\'t find that command. Try `r.help` to view a list of all commands.');
				return;
			}

			// send the message with help on the command
			message.channel.send({embed: {
				color: constants.EMBED_INFO,
				author: {
					name: _bot.bot.user.username,
					icon_url: _bot.bot.user.avatarURL
				},
				title: helpData.name,
				description: helpData.longDescription || helpData.description,
				fields: [
					{
						name: 'Usage',
						value: 'r.' + helpData.name + ' `' + helpData.usage.split('|').join(' `**|**` ') + '`'
					}
				],
				timestamp: new Date(),
				footer: {
					icon_url: _bot.bot.user.avatarURL,
					text: 'Discord Relics - A Hackweek Bot'
				}
			}});
		}
	}
};
