let _ = require('lodash');
let similarity = require('string-similarity');

let cards = require('./../cards.js');

module.exports = {
	name: 'item',
	documentation: {
		name: 'item',
		description: 'View an item',
		usage: '<name>'
	},
	execute: async function (args, message) {
		this._info(args, message);
	},
	_info: async function (args, message) {
		// fuzzy search
		let search = similarity.findBestMatch(args.join(' ').toLowerCase(), cards.all.filter(p => !p.hidden).map(c => c.name.toLowerCase()));
		let bestName = search.bestMatch.target;

		// fuzzy, but not too fuzzy
		if (search.bestMatch.rating < 0.8) {
			_util.embedNegative(message.channel, '', 'Couldn\'t find an item by that name. Check your spelling and try again.');
			return;
		}

		// find the pack they're searching for (name => id)
		let cardId = _.find(cards.all, c => c.name.toLowerCase() == bestName).id;
		let cardInfo = cards.byId[cardId];

		// prepare embed
		let fields = [];
		fields.push({
			name: 'Rarity',
			value: cards.rarities[cardInfo.rarity].toLowerCase(),
			inline: true
		});
		fields.push({
			name: 'Type',
			value: cardInfo.type,
			inline: true
		});
		fields.push({
			name: 'Sets',
			value: cardInfo.sets.filter(r => !cards.secretSets.includes(r) && r != 'all').join(', '),
			inline: true
		});

		// notify
		message.channel.send({
			embed: {
				color: cards.rarityColors[cardInfo.rarity],
				author: {
					name: _bot.bot.user.username,
					icon_url: _bot.bot.user.avatarURL
				},
				title: cardInfo.name,
				description: cardInfo.description,
				fields: fields,
				timestamp: new Date(),
				footer: {
					icon_url: _bot.bot.user.avatarURL,
					text: 'Discord Relics - A Hackweek Bot'
				}
			},
			files: [
				'./assets/' + cardInfo.asset + '.png'
			]
		});
	}
};
