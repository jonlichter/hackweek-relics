let _ = require('lodash');

let cards = require('./../cards.js');

let chunkSize = 20;

module.exports = {
	name: 'completion',
	documentation: {
		name: 'completion',
		description: 'View your collection completion progress.',
		usage: '[page = 1]'
	},
	execute: async function (args, message) {
		let profile = await _database.fetchUser(message.author.id);

		let page = args[0] - 1 || 0;

		if (page < 0) {
			_util.embedNegative(message.channel, '', 'Invalid page number.');
			return;
		}

		let hundredPercent = true;

		let pretty = [];
		let sets = cards.sets;
		sets.sort().forEach(function (setName) {
			let a = Object.keys(profile.inventory).filter(id => cards.byId[id].sets.includes(setName)).length;
			let b = cards.setSizes[setName];
			let progress = Math.round((a / b) * 10000) / 100;

			if (progress < 100) {
				hundredPercent = false;
			}

			if (!cards.secretSets.includes(setName)) {
				pretty.push('- ' + setName + ': ' + a + ' of ' + b + ' (' + progress + '%)');
			}
		});

		let chunked = _.chunk(pretty, chunkSize);

		if (page + 1 > chunked.length) {
			_util.embedNegative(message.channel, '', 'There are only ' + chunked.length + ' pages of sets.');
		}

		message.channel.send({embed: {
			color: constants.EMBED_INFO,
			author: {
				name: _bot.bot.user.username,
				icon_url: _bot.bot.user.avatarURL
			},
			title: message.member.nickname || message.author.username + '\'s Completion [Page ' + (page + 1) + ' of ' + chunked.length +']' + (hundredPercent ? ' **Complete!**' : ''),
			description: chunked[page].join('\n'),
			timestamp: new Date(),
			footer: {
				icon_url: _bot.bot.user.avatarURL,
				text: 'Discord Relics - A Hackweek Bot'
			}
		}});
	}
};
