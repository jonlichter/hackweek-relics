let _ = require('lodash');
let similarity = require('string-similarity');

let util = require('./../util.js');
let cards = require('./../cards.js');

module.exports = {
	name: 'gift',
	documentation: {
		name: 'gift',
		description: 'Gift another user items.',
		usage: '@user <item name>'
	},
	execute: async function (args, message) {
		if (args.length < 2) {
			_util.embedNegative(message.channel, '', 'Gift another user an item with `r.gift @user <item name>`');
			return;
		}

		let targetID = util.parseMention(args[0] || '<@1>');

		let target;
		try {
			target = await _bot.bot.fetchUser(targetID);
		} catch (e) {
			_util.embedNegative(message.channel, '', 'Unknown user.');
			return;
		}

		if (!target) {
			_util.embedNegative(message.channel, '', 'Unknown user.');
			return;
		}

		let fromProfile = await _database.fetchUser(message.author.id);
		let fromInv = fromProfile.inventory;

		// fuzzy search
		let searchTerm = args.slice(1).join(' ').toLowerCase();
		let search = similarity.findBestMatch(searchTerm, Object.keys(fromInv).map(c => cards.byId[c].name.toLowerCase()));
		let bestName = search.bestMatch.target;

		// fuzzy, but not too fuzzy
		if (search.bestMatch.rating < 0.8) {
			_util.embedNegative(message.channel, '', 'Couldn\'t find an item by that name. Check your spelling and try again.');
			return;
		}

		// find the pack they're searching for (name => id)
		let cardId = _.find(Object.keys(fromInv).map(i => cards.byId[i]), c => c.name.toLowerCase() == bestName).id;
		let cardInfo = cards.byId[cardId];

		if (!fromInv[cardId] || fromInv[cardId] < 1) {
			_util.embedNegative(message.channel, '', 'You don\'t have that item to gift!');
			return;
		}

		let toProfile = await _database.fetchUser(targetID);
		let toInv = toProfile.inventory;

		fromInv[cardId] = fromInv[cardId] - 1;
		toInv[cardId] = (toInv[cardId] || 0) + 1;

		await db.table('users').get(message.author.id).update({
			inventory: fromInv,
			stats: {
				cardsSent: (fromProfile.stats.cardsSent || 0) + 1
			}
		}).run(conn);

		await db.table('users').get(targetID).update({
			inventory: toInv,
			stats: {
				cardsReceived: (fromProfile.stats.cardsReceived || 0) + 1
			}
		}).run(conn);

		_util.embedPositive(message.channel, '', 'Sent **' + cardInfo.name + '** from **' + message.author.username + '** to **' + target.username + '**.');
	}
};
