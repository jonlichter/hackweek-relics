let util = require('./../util.js');

module.exports = {
	name: 'pay',
	documentation: {
		name: 'pay',
		description: 'Pay another user :dollar:s.',
		usage: '@user <amount>'
	},
	execute: async function (args, message) {
		let targetID = util.parseMention(args[0]);

		let target = await _bot.bot.fetchUser(targetID);

		if (!target) {
			message.channel.send('Unknown user.');
			return;
		}

		let amount = parseInt(args[1]);

		if (isNaN(amount)) {
			message.channel.send('Failed to parse amount.');
			return;
		}

		let fromProfile = await _database.fetchUser(message.author.id);
		if (fromProfile.money < amount) {
			message.channel.send('You don\'t have enough :dollar: to send!');
			return;
		}

		let toProfile = await _database.fetchUser(targetID);

		await db.table('users').get(message.author.id).update({
			money: fromProfile.money - amount,
			stats: {
				moneySent: (fromProfile.stats.moneySent || 0) + amount,
				transactionsSent: (fromProfile.stats.transactionsSent || 0) + 1
			}
		}).run(conn);

		await db.table('users').get(targetID).update({
			money: toProfile.money + amount,
			stats: {
				moneyReceived: (fromProfile.stats.moneyReceived || 0) + amount,
				transactionsReceived: (fromProfile.stats.transactionsReceived || 0) + 1
			}
		}).run(conn);

		message.channel.send('Sent **' + amount + '** :dollar: from **' + message.author.username + '** to **' + target.username + '**.');
	}
};
