let util = require('./../util');

module.exports = {
	name: 'eval',
	documentation: {
		name: 'eval',
		description: 'Evaluate code',
		usage: '<code>',
		hidden: true
	},
	execute: async function (args, message) {
		if (!util.isDev(message.author)) {
			message.channel.send(`**${message.member.nickname || message.author.username}**, you aren't a developer!`);
			return;
		}

		try {
			message.channel.send(eval(args.join(' ')));
		} catch(e) {
			console.log(`EVAL ERROR STACK: ${e.stack}`);
			message.channel.send(e.stack);
		}
	}
};
