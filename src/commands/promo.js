// promotion config
let currentPromos = {
	'hackweek2019': {
		money: 1000,
		packs: ['hackweek_2019_redeem'],
		cards: ['dhw_wumpus']
	},
	'thanks-for-reading': {
		money: 500,
		packs: [],
		cards: []
	}/*,
	'dev': {
		money: 1000000,
		packs: new Array(1000).fill('discord_1'),
		cards: ['test_card']
	}*/
};

module.exports = {
	name: 'promo',
	documentation: {
		name: 'promo',
		description: 'Claim current promotions',
		longDescription: 'Claim current promotions.\n\nThanks for reading the help! Use code `thanks-for-reading` for some free stuff!',
		usage: '<promotion>'
	},
	execute: async function (args, message) {
		// get the promotion info
		let promoName = args.join(' ');
		let promo = currentPromos[promoName];
		if (!promo) {
			_util.embedNegative(message.channel, '', 'Unknown promotion code.');
			return;
		}

		// make sure the user hasn't already claimed this promotion
		let profile = await _database.fetchUser(message.author.id);
		if (profile.promos[promoName]) {
			_util.embedNegative(message.channel, '', 'You already claimed that promotion.');
			return;
		}

		// give rewards
		let nPacks = profile.packs;
		promo.packs.forEach(function (pack) {
			nPacks[pack] = (nPacks[pack] || 0) + 1;
		});
		let nInv = profile.inventory;
		promo.cards.forEach(function (card) {
			nInv[card] = (nInv[card] || 0) + 1;
		});
		let nPromos = profile.promos;
		nPromos[promoName] = true;

		// update database
		await db.table('users').get(message.author.id).update({
			packs: nPacks,
			inventory: nInv,
			promos: nPromos,
			money: profile.money + (promo.money || 0)
		}).run(conn);

		// textual rewards list
		let rewards = [];
		if (promo.money != 0) {
			rewards.push(promo.money + ':dollar:');
		}
		if (promo.packs != []) {
			rewards.push(promo.packs.length + ' pack' + (promo.packs.length != 1 ? 's' : ''));
		}
		if (promo.cards != []) {
			rewards.push(promo.cards.length + ' card' + (promo.cards.length != 1 ? 's' : ''));
		}

		// notify
		_util.embedPositive(message.channel, '', 'Claimed promotion `' + promoName + '`! (' + rewards.join(', ') + ')');
	}
};
