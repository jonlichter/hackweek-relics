let _ = require('lodash');
let prettyMS = require('pretty-ms');

module.exports = {
	name: 'daily',
	documentation: {
		name: 'daily',
		description: 'Claim daily bot rewards (22h)',
		usage: ''
	},
	execute: async function (args, message) {
		// get information from database
		let profile = await _database.fetchUser(message.author.id);

		// time check, make sure it's been 22h
		let checkTime = 79200000;
		// standard variablessss
		let newTimeStamp = Date.now();
		let oldTimeStamp = profile.dailyTime || 0;
		// their payout is 100 (balance later) + a random value 1-20
		let payOut = 100 + _.random(1, 20, false);
		// the player will get their next payout in...
		let nextPayout = prettyMS(checkTime - (newTimeStamp - oldTimeStamp));

		if((newTimeStamp - oldTimeStamp) > checkTime) {
			//reset their timestamp
			await db.table('users').get(message.author.id).update({
				dailyTime: newTimeStamp,
				money: profile.money + payOut,
				stats: {
					dailyClaimed: (profile.stats.dailyClaimed || 0) + 1
				}
			}).run(conn);
			_util.embedPositive(message.channel, 'Daily Claimed!', 'You got ' + payOut + ':dollar:!\nNext daily reward is available in about 22 hours.');
		} else {
			_util.embedNegative(message.channel, 'Daily is on cooldown!', 'Next daily reward is available in ' + nextPayout + '.');
		}
	}
};
