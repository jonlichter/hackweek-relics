module.exports = {
	name: 'profile',
	documentation: {
		name: 'profile',
		description: 'See your profile and stats',
		usage: ''
	},
	execute: async function (args, message) {
		this.displayStats(message.channel, message.author.id, message.author);
	},
	displayStats: async function (channel, userId, user) {
		let u;
		try {
			u = user || await _bot.bot.fetchUser(userId);
		} catch (e) {
			_util.embedNegative(channel, '', 'Unknown user.');
			return;
		}
		let profile = await _database.fetchUser(userId);

		let stats = [
			(profile.stats.itemDropsClaimed || 0) + ' item drops claimed',
			(profile.stats.packsOpened || 0) + ' packs opened',
			Object.values(profile.inventory).reduce((a, b) => a + b, 0) + ' items owned (' + Object.keys(profile.inventory).length + ' unique)'
		];

		if ((profile.stats.cardguessStarted || 0) > 0) {
			stats.push((Math.round(((profile.stats.cardguessWon || 0) / (profile.stats.cardguessStarted)) * 10000) / 100) + '% CardGuess win rate');
		} else {
			stats.push('Unknown CardGuess win rate');
		}

		await channel.send({embed: {
			color: constants.EMBED_INFO,
			author: {
				name: _bot.bot.user.username,
				icon_url: _bot.bot.user.avatarURL
			},
			title: u.username + '\'s Profile',
			description: profile.money + ':dollar:',
			fields: [
				{
					name: 'Stats',
					value: stats.join('\n')
				},
			],
			timestamp: new Date(),
			footer: {
				icon_url: _bot.bot.user.avatarURL,
				text: 'Discord Relics - A Hackweek Bot'
			}
		}});
	}
};
