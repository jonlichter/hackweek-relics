let _ = require('lodash');
let similarity = require('string-similarity');

let cards = require('./../cards.js');
let packs = require('./../packs.js');

let chunkSize = 4;

let offers = [{
	name: 'Discord Set Pack',
	input: {
		money: 150
	},
	output: {
		packs: ['discord_1']
	}
}, /*{
	name: 'Discord Set Pack (Bulk)',
	input: {
		money: 1500
	},
	output: {
		packs: new Array(15).fill('discord_1')
	}
}, */{
	name: 'Hypesquad House Bundle',
	input: {
		money: 500
	},
	output: {
		items: ['discord_house_balance', 'discord_house_bravery', 'discord_house_brilliance']
	}
}];

module.exports = {
	name: 'shop',
	documentation: {
		name: 'shop',
		description: 'View and buy offers in the shop',
		usage: 'view [page = 1] | buy <name>'
	},
	execute: async function (args, message) {
		// select subcommand
		let subcommand = (args.shift() || '').toLowerCase();
		if (subcommand == 'view' || subcommand == 'v' || subcommand == 'list' || subcommand == 'l') {
			this._view(args, message);
		} else if (subcommand == 'buy' || subcommand == 'b') {
			this._buy(args, message);
		} else {
			_util.embedNegative(message.channel, '', 'Try `r.shop view` or `r.shop buy <name>`');
		}
	},
	_buy: async function (args, message) {
		// get information from database
		let profile = await _database.fetchUser(message.author.id);

		// fuzzy search
		let search = similarity.findBestMatch(args.join(' ').toLowerCase(), offers.map(o => o.name.toLowerCase()));
		let bestName = search.bestMatch.target;

		// fuzzy, but not too fuzzy
		if (search.bestMatch.rating < 0.7) {
			_util.embedNegative(message.channel, '', 'Couldn\'t find an offer by that name. Check your spelling and try again.');
			return;
		}

		// find the pack they're searching for (name => id)
		let offer = _.find(offers, o => o.name.toLowerCase() == bestName);

		// check money
		let newMoney = profile.money;
		if (offer.input.money) {
			if (profile.money < offer.input.money) {
				_util.embedNegative(message.channel, '', 'You don\'t have enough :dollar: to buy that.');
				return;
			} else {
				newMoney = newMoney - offer.input.money;
			}
		}

		// give packs
		let newPacks = profile.packs;
		if (offer.output.packs) {
			offer.output.packs.forEach(function (packId) {
				newPacks[packId] = (newPacks[packId] || 0) + 1;
			});
		}

		// give items
		let newInv = profile.inventory;
		if (offer.output.items) {
			offer.output.items.forEach(function (itemId) {
				newInv[itemId] = (newInv[itemId] || 0) + 1;
			});
		}

		// update database
		await db.table('users').get(message.author.id).update({
			money: newMoney,
			packs: newPacks,
			inventory: newInv
		}).run(conn);

		// notify
		let take = [];
		if (offer.input.money) {
			take.push(offer.input.money + ' :dollar:');
		}
		if (take.length < 1) {
			take.push('nothing');
		}

		let get = [];
		if (offer.output.packs) {
			let packCount = offer.output.packs.length;
			get.push(packCount + ' pack' + (packCount != 1 ? 's' : ''));
		}
		if (offer.output.items) {
			let itemCount = offer.output.items.length;
			get.push(itemCount + ' item' + (itemCount != 1 ? 's' : ''));
		}
		if (get.length < 1) {
			get.push('nothing');
		}

		_util.embedPositive(message.channel, '', 'You bought **' + offer.name + '** for ' + take.join(', ') + ' and got ' + get.join(', ') + '!');
	},
	_view: async function (args, message) {
		// page checks
		let page = args[0] - 1 || 0;
		if (page < 0) {
			_util.embedNegative(message.channel, '', 'Invalid page number.');
			return;
		}

		let pretty = [];

		// prettify
		offers.forEach(function (offer) {
			let costs = [];
			if (offer.input.money) {
				costs.push(offer.input.money + ' :dollar:');
			}

			let get = [];
			if (offer.output.packs) {
				offer.output.packs.forEach(function (packId) {
					get.push(packs.byId[packId].name);
				});
			}
			if (offer.output.items) {
				offer.output.items.forEach(function (cardId) {
					get.push(cards.byId[cardId].name);
				});
			}

			pretty.push('**' + offer.name + '**\nCost: ' + costs.join(', ') + '\n' + get.map(g => ' - ' + g).join('\n'));
		});

		// split into pages
		let chunked = _.chunk(pretty, chunkSize);

		// check max pages
		if (page + 1 > chunked.length) {
			_util.embedNegative(message.channel, '', 'There are only ' + chunked.length + ' shop pages.');
		}

		// notify
		message.channel.send({embed: {
			color: constants.EMBED_INFO,
			author: {
				name: _bot.bot.user.username,
				icon_url: _bot.bot.user.avatarURL
			},
			title: 'Shop [Page ' + (page + 1) + ' of ' + chunked.length +']',
			description: chunked[page].join('\n'),
			timestamp: new Date(),
			footer: {
				icon_url: _bot.bot.user.avatarURL,
				text: 'Discord Relics - A Hackweek Bot'
			}
		}});
	}
};
