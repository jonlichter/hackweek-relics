let similarity = require('string-similarity');
let _ = require('lodash');

let distortEngine = require('./../distort/engine.js');
let cards = require('./../cards');

let buyIn = 25;
let payOut = 50;

function guessEmojis(num) {
	return ':black_square_button:'.repeat(num) + ':black_large_square:'.repeat(3 - num);
}

module.exports = {
	games: {},

	name: 'cardguess',
	documentation: {
		name: 'cardguess',
		description: 'Pay ' + buyIn + ' :dollar: and receive ' + payOut + ' :dollar: if you can guess the card in 3 guesses.',
		usage: 'start | <your guess>'
	},
	execute: async function (args, message) {
		// subcommand
		if (this.games[message.author.id]) {
			this._guess(args, message);
		} else if (args[0] == 'start') {
			this._start(args, message);
		} else {
			_util.embedNegative(message.channel, '', 'Start a CardGuess game with `r.cardguess start` and guess with `r.cardguess <guess>`');
		}
	},
	_start: async function (args, message) {
		// game data
		let card = _.sample(cards.bySet.cg);
		this.games[message.author.id] = {
			card: card,
			guessesRemaining: 3
		};

		// distort
		let imageId = await distortEngine.distort(card.asset, 3);

		// get information from database
		let profile = await _database.fetchUser(message.author.id);

		// check buy-in
		if (profile.money < 25) {
			_util.embedNegative(message.channel, '', 'You don\'t have ' + buyIn + ' :dollar: to start a cardguess game.');
			return;
		}

		// stat tracker
		let cardguessStarted = ((profile.stats || {}).cardguessStarted || 0) + 1;

		// update database
		await db.table('users').get(message.author.id).update({
			money: profile.money - buyIn,
			stats: {
				cardguessStarted: cardguessStarted
			}
		}).run(conn);

		message.channel.send({
			embed: {
				author: {
					name: _bot.bot.user.username,
					icon_url: _bot.bot.user.avatarURL
				},
				color: constants.EMBED_INFO,
				title: 'CardGuess!',
				description: '*-' + buyIn + ' :dollar:* ' + (message.member.nickname || message.author.username) + ', use `r.cardguess <your guess>` to guess. You have ' + this.games[message.author.id].guessesRemaining + ' guesses remaining.\n\n' + guessEmojis(3),
				timestamp: new Date(),
				footer: {
					icon_url: _bot.bot.user.avatarURL,
					text:'Discord Relics - A Hackweek Project'
				}
			},
			files: [
				'./distorted/' + imageId + '.png'
			]
		});
	},
	_guess: async function (args, message) {
		let game = this.games[message.author.id];
		if (!game) {
			console.log('guessing but has no game? failing silently');
			return;
		}

		let sim = similarity.compareTwoStrings(game.card.name.toLowerCase(), args.join(' ').toLowerCase());
		console.log('similarity report: "' + game.card.name.toLowerCase() + '", "' + args.join(' ').toLowerCase() +'", ' + sim);

		let card = this.games[message.author.id].card;

		if (sim > 0.8) {
			this.games[message.author.id] = null;

			// get information from database
			let profile = await _database.fetchUser(message.author.id);

			// stat tracker
			let cardguessWon = ((profile.stats || {}).cardguessWon || 0) + 1;

			// update database
			await db.table('users').get(message.author.id).update({
				money: profile.money + payOut,
				stats: {
					cardguessWon: cardguessWon
				}
			}).run(conn);

			await message.channel.send({
				embed: {
					author: {
						name: _bot.bot.user.username,
						icon_url: _bot.bot.user.avatarURL
					},
					color: constants.EMBED_POSITIVE,
					title: '',
					description: (message.member.nickname || message.author.username) + ' guessed the card! It was **' + card.name + '**. ' + payOut + ' :dollar: has been added to their account.',
					timestamp: new Date(),
					footer: {
						icon_url: _bot.bot.user.avatarURL,
						text:'Discord Relics - A Hackweek Project'
					}
				},
				files: [
					'./assets/' + card.asset + '.png'
				]
			});
		} else {
			// get information from database
			let profile = await _database.fetchUser(message.author.id);

			// stat tracker
			let cardguessGuess = ((profile.stats || {}).cardguessGuess || 0) + 1;

			// update database
			await db.table('users').get(message.author.id).update({
				stats: {
					cardguessGuessed: cardguessGuess
				}
			}).run(conn);

			if (game.guessesRemaining == 1) {
				this.games[message.author.id] = null;
				await message.channel.send({
					embed: {
						author: {
							name: _bot.bot.user.username,
							icon_url: _bot.bot.user.avatarURL
						},
						color: constants.EMBED_NEGATIVE,
						title: '',
						description: 'Sorry ' + (message.member.nickname || message.author.username) + ', you used all your guesses. The card was **' + card.name + '**. As you collect and see more and more cards, you\'ll get better at guessing the card. Better luck next time!',
						timestamp: new Date(),
						footer: {
							icon_url: _bot.bot.user.avatarURL,
							text:'Discord Relics - A Hackweek Project'
						}
					},
					files: [
						'./assets/' + card.asset + '.png'
					]
				});
			} else {
				this.games[message.author.id].guessesRemaining -= 1;
				_util.embedInfo(message.channel, '', 'Wrong card, ' + (message.member.nickname || message.author.username) + '. You have ' + this.games[message.author.id].guessesRemaining + ' guesses remaining.\n\n' + guessEmojis(this.games[message.author.id].guessesRemaining));
			}
		}
	}
};
