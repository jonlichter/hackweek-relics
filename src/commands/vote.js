module.exports = {
	name: 'vote',
	documentation: {
		name: 'vote',
		description: 'Vote for the bot.',
		usage: null
	},
	execute: async function (args, message) {
		let client = _bot.bot;

		message.channel.send({
			embed: {
				author: {
					name: client.user.username,
					icon_url: client.user.avatarURL
				},
				color: constants.EMBED_POSITIVE,
				title: 'Vote for Discord Relics',
				url: 'https://discordbots.org/bot/592804506908098572/vote',
				description: 'Thanks for voting for us! Rewards will be implemented in the future.',
				timestamp: new Date(),
				footer: {
					icon_url: client.user.avatarURL,
					text:'Discord Relics - A Hackweek Project'
				}
			}
		});
	}
};
