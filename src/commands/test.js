let distortEngine = require('./../distort/engine.js');

module.exports = {
	name: 'test',
	documentation: {
		name: 'test',
		description: 'Distortion test',
		usage: '',
		hidden: true
	},
	execute: async function (args, message) {
		// disabled
		let t = true;
		if (t) {
			return;
		}

		let resultId = await distortEngine.distort(args[1] || 'card_test', args[0] || 2);

		message.channel.send('Test distortion:', {
			files: [
				'./distorted/' + resultId + '.png'
			]
		});
	}
};
