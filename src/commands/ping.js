module.exports = {
	name: 'ping',
	documentation: {
		name: 'ping',
		description: 'Check bot and server response times',
		usage: ''
	},
	execute: async function (args, message) {
		let client = _bot.bot;

		let m = await message.channel.send('*pinging...*');
		m.edit({embed: {
			color: constants.EMBED_INFO,
			author: {
				name: client.user.username,
				icon_url: client.user.avatarURL
			},
			title: 'Pong!',
			description: 'Time taken: ' + (m.createdTimestamp - message.createdTimestamp) + 'ms',
			timestamp: new Date(),
			footer: {
				icon_url: client.user.avatarURL,
				text: 'Discord Relics - A Hackweek Bot'
			}
		}});
	}
};
