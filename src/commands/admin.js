let util = require('./../util');
let distort = require('./../distort/engine.js');

let profileHandler = require('./profile');

let prettyMS = require('pretty-ms');
let osu = require('node-os-utils');
let utilLib = require('util');
let exec = utilLib.promisify(require('child_process').exec);

module.exports = {
	name: 'admin',
	documentation: {
		name: 'admin',
		description: 'Run admin tasks',
		usage: '<task>',
		hidden: true
	},
	execute: async function (args, message) {
		if (!util.isDev(message.member)) {
			// message.channel.send(`**${message.member.nickname || message.author.username}**, you aren't a developer!`);
			return;
		}

		switch((args[0] || '').toLowerCase()) {
		case 'update': {
			let m = await message.channel.send('*executing `git pull`...*');
			let { stdout } = await exec('cd ' + process.cwd() + ' && git pull');
			m.edit('Result: ```\n' + stdout + '\n```');
			break;
		}

		case 'clean': {
			let m = await message.channel.send('*cleaning...*');
			await distort.clean();
			m.edit('*cleaning done*');
			break;
		}

		case 'restart_shard': {
			await message.channel.send('*restarting shard...*');
			_bot.bot.shard.send({
				action: 'restartShard'
			});
			break;
		}

		case 'restart': {
			await message.channel.send('*restarting all shards... (will reload code)*');
			_bot.bot.shard.send({
				action: 'restart'
			});
			break;
		}

		case 'shutdown': {
			await message.channel.send('*shutting down shard manager...*');
			_bot.bot.shard.send({
				action: 'shutdown'
			});
			break;
		}

		case 'view_user': {
			let id = args[1];
			profileHandler.displayStats(message.channel, id);
			break;
		}

		case 'reset_user': {
			let id = args[1];
			let discordUser = await _bot.bot.fetchUser(id);
			if (!discordUser) {
				message.channel.send('Invalid Discord user ID.');
				return;
			}
			await db.table('users').get(id).delete().run(conn);
			message.channel.send('Reset ' + discordUser.username + '\'s stats.');
			break;
		}

		case 'dump_user': {
			let id = args[1];
			let data = await _database.fetchUser(id);
			message.channel.send(utilLib.inspect(data));
			break;
		}

		case 'stats': {
			let registeredUsers = await db.table('users').count().run(conn);
			let registeredGuilds = await db.table('guilds').count().run(conn);
			message.channel.send({
				embed: {
					author: {
						name: _bot.bot.user.username,
						icon_url: _bot.bot.user.avatarURL
					},
					color: constants.EMBED_INFO,
					title: 'Admin Stats — Bot',
					description: '',
					fields: [{
						name: 'Guilds',
						value: (await _bot.bot.shard.fetchClientValues('guilds.size')).reduce((a, b) => a + b, 0),
						inline: true
					}, {
						name: 'Users',
						value: (await _bot.bot.shard.fetchClientValues('users.size')).reduce((a, b) => a + b, 0),
						inline: true
					}, {
						name: 'Database Users',
						value: registeredUsers,
						inline: true
					}, {
						name: 'Database Guilds',
						value: registeredGuilds,
						inline: true
					}, {
						name: 'Shard',
						value: _bot.bot.shard.id + ' of ' + _bot.bot.shard.count,
						inline: true
					}, {
						name: 'Uptime',
						value: prettyMS(process.uptime() * 1000),
						inline: true
					}],
					timestamp: new Date(),
					footer: {
						icon_url: _bot.bot.user.avatarURL,
						text:'Discord Relics - A Hackweek Project'
					}
				}
			});
			break;
		}

		case 'stats_server': {
			let driveInfo = await osu.drive.info();
			let memInfo = await osu.mem.info();

			message.channel.send({
				embed: {
					author: {
						name: _bot.bot.user.username,
						icon_url: _bot.bot.user.avatarURL
					},
					color: constants.EMBED_INFO,
					title: 'Admin Stats — Server',
					description: '',
					fields: [{
						name: 'OS',
						value: (await osu.os.oos()) + ' (' + osu.os.arch() + ')',
						inline: true
					}, {
						name: 'CPU',
						value: (await osu.cpu.usage()) + '%',
						inline: true
					}, {
						name: 'Drive',
						value: driveInfo.usedGb + 'gb / ' + driveInfo.totalGb + ' (' + driveInfo.usedPercentage + '%)',
						inline: true
					}, {
						name: 'Memory',
						value: memInfo.usedMemMb + 'mb / ' + memInfo.totalMemMb + 'mb (' + memInfo.freeMemPercentage + '% free)',
						inline: true
					}, {
						name: 'Uptime',
						value: prettyMS(osu.os.uptime() * 1000),
						inline: true
					}],
					timestamp: new Date(),
					footer: {
						icon_url: _bot.bot.user.avatarURL,
						text:'Discord Relics - A Hackweek Project'
					}
				}
			});
			break;
		}

		default: {
			message.channel.send('`' + ['update', 'stats', 'stats_server', 'clean', 'restart_shard', 'restart', 'shutdown', 'view_user', 'reset_user', 'dump_user'].join('`, `') + '`');
			break;
		}
		}
	}
};

/*
{
	name: 'Users',
	value: _bot.bot.users.size,
	inline: true
},
{
	name: 'Servers',
	value: _bot.bot.guilds.size,
	inline: true
},
*/
