let _ = require('lodash');

let cards = require('./../cards.js');

let chunkSize = 20;

module.exports = {
	name: 'inventory',
	documentation: {
		name: 'inventory',
		description: 'View your item inventory',
		usage: '[page = 1]'
	},
	execute: async function (args, message) {
		let profile = await _database.fetchUser(message.author.id);

		let page = args[0] - 1 || 0;

		if (page < 0) {
			_util.embedNegative(message.channel, '', 'Invalid page number.');
			return;
		}

		let sumCards = Object.values(profile.inventory).reduce((a, b) => a + b, 0);
		if (sumCards == 0) {
			_util.embedNegative(message.channel, '', 'You don\'t have any items to view.');
			return;
		}

		let pretty = [];
		Object.keys(profile.inventory).sort().forEach(function (k) {
			let v = profile.inventory[k];

			let card = cards.byId[k];

			if (v < 1) {
				return;
			}

			pretty.push('- ' + card.name + (v > 1 ? ' [x' + v + ']' : '') + ' [' + cards.rarityShort[card.rarity] + ']');
		});

		let chunked = _.chunk(pretty, chunkSize);

		if (page + 1 > chunked.length) {
			_util.embedNegative(message.channel, '', 'You only have ' + chunked.length + ' pages of items.');
		}

		_util.embedInfo(message.channel, message.member.nickname || message.author.username + '\'s Inventory [Page ' + (page + 1) + ' of ' + chunked.length +']', chunked[page].join('\n'));
	}
};
