let _ = require('lodash');
let similarity = require('string-similarity');

let cards = require('./../cards.js');
let packs = require('./../packs.js');

let chunkSize = 20;

module.exports = {
	name: 'pack',
	documentation: {
		name: 'pack',
		description: 'View and open packs',
		usage: 'list [page = 1] | open <name> | info <name>'
	},
	execute: async function (args, message) {
		// select subcommand
		let subcommand = (args.shift() || '').toLowerCase();
		if (subcommand == 'list' || subcommand == 'l') {
			this._list(args, message);
		} else if (subcommand == 'open' || subcommand == 'o') {
			this._open(args, message);
		} else if (subcommand == 'info' || subcommand == 'view' || subcommand == 'i' || subcommand == 'v') {
			this._info(args, message);
		} else {
			_util.embedNegative(message.channel, '', 'Try `r.pack list [page]`, `r.pack open <name>`, or `r.pack info <name>`');
		}
	},
	_info: async function (args, message) {
		// fuzzy search
		let search = similarity.findBestMatch(args.join(' ').toLowerCase(), packs.packs.filter(p => !p.hidden).map(p => p.name.toLowerCase()));
		let bestName = search.bestMatch.target;

		// fuzzy, but not too fuzzy
		if (search.bestMatch.rating < 0.8) {
			_util.embedNegative(message.channel, '', 'Couldn\'t find a pack by that name. Check your spelling and try again.');
			return;
		}

		// find the pack they're searching for (name => id)
		let packId = _.find(packs.packs, p => p.name.toLowerCase() == bestName).id;
		let packInfo = packs.byId[packId];

		// prepare embed
		let fields = [];
		if (packInfo.included.length > 0) {
			fields.push({
				name: 'Included Items',
				value: packInfo.included.map(c => ' - ' + cards.byId[c].name).join('\n')
			});
		}
		if (packInfo.randomAmount > 0) {
			fields.push({
				name: 'Possible Items (' + packInfo.randomAmount + ')',
				value: packInfo.random.map(c => ' - ' + cards.byId[c].name).join('\n')
			});
		}

		// notify
		message.channel.send({embed: {
			color: constants.EMBED_INFO,
			author: {
				name: _bot.bot.user.username,
				icon_url: _bot.bot.user.avatarURL
			},
			title: packInfo.name,
			description: packInfo.description,
			fields: fields,
			timestamp: new Date(),
			footer: {
				icon_url: _bot.bot.user.avatarURL,
				text: 'Discord Relics - A Hackweek Bot'
			}
		}});
	},
	_open: async function (args, message) {
		// get information from database
		let profile = await _database.fetchUser(message.author.id);

		// check that they have some packs to open
		let packNames = Object.keys(profile.packs).map(id => packs.byId[id].name);
		if (packNames.length < 1) {
			_util.embedNegative(message.channel, '', 'You don\'t have any packs to open.');
			return;
		}

		// fuzzy search
		let search = similarity.findBestMatch(args.join(' ').toLowerCase(), packNames.map(p => p.toLowerCase()));
		let bestName = search.bestMatch.target;

		// fuzzy, but not too fuzzy
		if (search.bestMatch.rating < 0.8) {
			_util.embedNegative(message.channel, '', 'Couldn\'t find a pack by that name. Check your spelling and try again.');
			return;
		}

		// find the pack they're searching for (name => id)
		let packId = _.find(packs.packs, p => p.name.toLowerCase() == bestName).id;
		let packInfo = packs.byId[packId];

		// double check that they have that pack
		if (profile.packs[packId] < 1) {
			_util.embedNegative(message.channel, '', 'You don\'t have any of that pack to open.');
			return;
		}

		// decrement pack in inventory
		let newPacks = profile.packs;
		newPacks[packId] = newPacks[packId] - 1;

		// randomize pack drops + guaranteeds
		let results = packs.open(packId);

		// add to inventory
		let newInv = profile.inventory;
		results.forEach(function (item) {
			newInv[item] = (newInv[item] || 0) + 1;
		});

		// stat tracker
		let packsOpened = ((profile.stats || {}).packsOpened || 0) + 1;

		// update database
		await db.table('users').get(message.author.id).update({
			packs: newPacks,
			inventory: newInv,
			stats: {
				packsOpened: packsOpened
			}
		}).run(conn);

		// notify
		message.channel.send({embed: {
			color: constants.EMBED_POSITIVE,
			author: {
				name: _bot.bot.user.username,
				icon_url: _bot.bot.user.avatarURL
			},
			title: message.member.nickname || message.author.username + ' Opens ' + packInfo.name,
			description: results.map(r => ' - ' + cards.byId[r].name).join('\n'),
			timestamp: new Date(),
			footer: {
				icon_url: _bot.bot.user.avatarURL,
				text: 'Discord Relics - A Hackweek Bot'
			}
		}});
	},
	_list: async function (args, message) {
		// get from database
		let profile = await _database.fetchUser(message.author.id);

		// page checks
		let page = args[0] - 1 || 0;
		if (page < 0) {
			_util.embedNegative(message.channel, '', 'Invalid page number.');
			return;
		}

		// check that they have some packs to view
		let sumPacks = Object.values(profile.packs).reduce((a, b) => a + b, 0);
		if (sumPacks == 0) {
			_util.embedNegative(message.channel, '', 'You don\'t have any packs to view.');
			return;
		}

		let pretty = [];

		// prettify
		Object.keys(profile.packs).sort().forEach(function (k) {
			let v = profile.packs[k];

			let pack = packs.byId[k];

			if (v < 1) {
				return;
			}

			pretty.push('- ' + pack.name + (v > 1 ? ' [x' + v + ']' : ''));
		});

		// split into pages
		let chunked = _.chunk(pretty, chunkSize);

		// check max pages
		if (page + 1 > chunked.length) {
			_util.embedNegative(message.channel, '', 'You only have ' + chunked.length + ' pages of packs.');
		}

		// notify
		message.channel.send({embed: {
			color: constants.EMBED_INFO,
			author: {
				name: _bot.bot.user.username,
				icon_url: _bot.bot.user.avatarURL
			},
			title: message.member.nickname || message.author.username + '\'s Packs [Page ' + (page + 1) + ' of ' + chunked.length +']',
			description: chunked[page].join('\n'),
			timestamp: new Date(),
			footer: {
				icon_url: _bot.bot.user.avatarURL,
				text: 'Discord Relics - A Hackweek Bot'
			}
		}});
	}
};
