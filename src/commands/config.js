let Discord = require('discord.js');

let util = require('./../util');

module.exports = {
	name: 'config',
	documentation: {
		name: 'config',
		description: 'Configure the bot for your server.',
		usage: 'itemdrop [true / false]'
	},
	execute: async function (args, message) {
		if (message.member.hasPermission(Discord.Permissions.FLAGS.MANAGE_GUILD, null, true, true) && !util.isDev(message.author)) {
			_util.embedNegative(message.channel, '', 'You must have the `MANAGE_SERVER` permission to change server config variables.');
			return;
		}

		if (!args[0]) {
			_util.embedNegative(message.channel, '', 'Use `r.config itemdrop false` to disable random item drops.');
		} else {
			let flag = args[0].toLowerCase();

			// list of config variables
			if (!['itemdrop'].includes(flag)) {
				_util.embedNegative(message.channel, '', 'Unknown config flag.');
				return;
			}

			if (args.length > 1) {
				// set values
				let target = args[1];

				if (['true', 't', 'yes', 'y', 'enabled'].includes(target)) {
					target = true;
				} else if (['false', 'f', 'no', 'n', 'disabled'].includes(target)) {
					target = false;
				} else {
					_util.embedNegative(message.channel, '', 'Unknown config value. Try `true` or `false`.');
					return;
				}

				await db.table('guilds').get(message.guild.id).update({
					flags: {
						[flag]: target
					}
				}).run(conn);

				_util.embedPositive(message.channel, 'Config: ' + flag, 'New value: ' + target);
			} else {
				// display values
				let currentState = await _database.fetchGuild(message.guild.id);

				_util.embedInfo(message.channel, 'Config: ' + flag, 'Current value: ' + currentState.flags[flag]);
			}
		}
	}
};
