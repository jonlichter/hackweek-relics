let cardDrop = require('./../cardDrop.js');

module.exports = {
	name: 'claim',
	documentation: {
		name: 'claim',
		description: 'Claim random item drops',
		usage: '<your guess>',
		hidden: true
	},
	execute: async function (args, message) {
		let result = cardDrop.checkClaim(message.channel, args.join(' '));

		if (result.success == 1) {
			// get information from database
			let profile = await _database.fetchUser(message.author.id);

			// add to inventory
			let newInv = profile.inventory;
			newInv[result.cardId] = (newInv[result.cardId] || 0) + 1;

			// stat tracker
			let claimed = ((profile.stats || {}).itemDropsClaimed || 0) + 1;

			// update database
			await db.table('users').get(message.author.id).update({
				inventory: newInv,
				stats: {
					itemDropsClaimed: claimed
				}
			}).run(conn);

			// MAKE SURE SUCCESSFULLY GAVE, then clear drop
			cardDrop.claimDrop(message.channel);

			await message.channel.send({
				embed: {
					author: {
						name: _bot.bot.user.username,
						icon_url: _bot.bot.user.avatarURL
					},
					color: constants.EMBED_POSITIVE,
					title: 'Item Drop!',
					description: (message.member.nickname || message.author.username) + ' guessed the item! It has been added to their inventory.',
					timestamp: new Date(),
					footer: {
						icon_url: _bot.bot.user.avatarURL,
						text:'Discord Relics - A Hackweek Project'
					}
				},
				files: [
					'./assets/' + result.asset + '.png'
				]
			});
		} else if (result.success == 0) {
			_util.embedNegative(message.channel, 'Item Drop!', 'Wrong card, ' + (message.member.nickname || message.author.username) + '. Guess again with `r.claim <your guess>`');
		} else if (result.success == -1) {
			// no drop currently, fail silently
		}
	}
};
