let prettyMilliseconds = require('pretty-ms');

module.exports = {
	name: 'info',
	documentation: {
		name: 'info',
		description: 'See information about the bot.',
		usage: null
	},
	execute: async function (args, message) {
		let client = _bot.bot;

		let uppertimer = prettyMilliseconds(client.uptime);

		message.channel.send({content: 'Join the Discord Relics bot server for updates, suggestions/bugs, and more! <https://discord.gg/neJkBv4>',
			embed: {
				color: constants.EMBED_INFO,
				author: {
					name: client.user.username,
					icon_url: client.user.avatarURL
				},
				title: 'Discord Relics',
				description: 'I\'m Discord Relics, a bot made for the Discord Hackweek 2019.',
				fields: [
					{
						name: 'Server Invite',
						value: 'Join the official Discord Relics server! https://discord.gg/neJkBv4'
					},
					{
						name: 'Bot Invite',
						value: 'Add me to your server: <https://discordapp.com/oauth2/authorize?client_id=' + client.user.id + '&scope=bot&permissions=35840>'
					},
					{

						name: 'Developers',
						value: '`notme#1560`, `RJain#8976`, and `avistah#2485`'
					},
					{
						name: 'Prefix',
						value: '`r.help`, `relic help`, or <@' + client.user.id + '>` help`'
					},
					{
						name: 'Uptime',
						value: uppertimer,
						inline: true
					},
					{
						name: 'Shard',
						value: client.shard.id,
						inline: true
					}
				],
				timestamp: new Date(),
				footer: {
					icon_url: client.user.avatarURL,
					text:'Discord Relics - A Hackweek Project'
				}
			}
		});
	}
};
