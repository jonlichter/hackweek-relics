let packs = [
	{
		id: 'example_pack',
		name: 'Example Pack Name',
		description: 'An example pack.',
		included: ['included-card-1', 'included-card-2'],
		randomAmount: 10,
		random: ['possible-card', 'another-possible-card', 'third-possible-card'],
		hidden: true
	},

	{
		id: 'hackweek_2019_redeem',
		name: 'Discord Hack Week 2019 Giveaway',
		description: 'Giving away Hack Week items from Discord Hack Week 2019.',
		included: ['dhw_souvenir'],
		randomAmount: 0,
		random: []
	},

	{
		id: 'discord_1',
		name: 'Discord Set',
		description: 'Includes 3 random Discord-themed cards.',
		included: [],
		randomAmount: 3,
		random: [
			'discord_announcements',
			'discord_billing',
			'discord_captcha',
			'discord_chat',
			'discord_empathybanana',
			'discord_faq',
			'discord_gamedev',
			'discord_gamestore',
			'discord_interface',
			'discord_nitroboost',
			'discord_nitroclassicwumpus',
			'discord_nitrowumpus',
			'discord_programs',
			'discord_ragingdemon',
			'discord_serversetup',
			'discord_settings',
			'discord_streaming',
			'discord_trustsafety',
			'discord_twitter',
			'discord_house_brilliance',
			'discord_house_bravery',
			'discord_house_balance',
			'dhw_wumpus'
		]
	}
];

let _ = require('lodash');

let byId = {};

// process by-id listing
packs.forEach(function (pack) {
	byId[pack.id] = pack;
});

module.exports = {
	packs: packs,
	byId: byId,

	find: function (search) {
		return _.find(packs, search);
	},

	open: function (packId) {
		let pack = this.byId[packId];

		let gotten = _.clone(pack.included) || [];

		for (let i = 0; i < pack.randomAmount; i++) {
			gotten.push(_.sample(pack.random));
		}

		return gotten;
	}
};
