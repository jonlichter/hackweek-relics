let fs = require('fs');
let gm = require('gm');
let nanoid = require('nanoid');
let _ = require('lodash');

let filterDifficulties = [
	// all difficulties
	[/*'rotate'*/],
	// difficulty 1,
	['implode', 'blur', 'charcoal', 'cycle', 'edge', 'emboss', 'paint', 'spread'],
	// difficulty 2,
	['colorize', 'swirl', 'negate', 'shade'],
	// difficulty 3
	[]
];

// each difficulty should include the lower difficulties
let filterPools = [
	[...filterDifficulties[0]],
	[...filterDifficulties[0], ...filterDifficulties[1]],
	[...filterDifficulties[0], ...filterDifficulties[1], ...filterDifficulties[2]],
	[...filterDifficulties[0], ...filterDifficulties[1], ...filterDifficulties[2], ...filterDifficulties[3]],
];

module.exports = {
	// Delete all files in the distorted directory and wait for it to finish before starting the bot
	// Ignore .gitkeep so git keeps the directory
	clean: function () {
		return new Promise(function(resolve, reject) {
			fs.readdir('./distorted/', async function (err, files) {
				if (err) {
					reject(err);
				}

				let len = files.length;

				for (let i = 0; i < len; i++) {
					let filename = files[i];

					if (filename === '.gitkeep') {
						continue;
					}

					await this._delete('./distorted/' + filename);
				}

				resolve();
			}.bind(this));
		}.bind(this));
	},
	// Helper wrapper function to make fs.unlink async
	_delete: function (path) {
		return new Promise(function(resolve, reject) {
			fs.unlink(path, function (err) {
				if (err) {
					reject(err);
				}

				resolve();
			});
		});
	},

	// Distort an image
	distort: function (sourceImage, difficulty) {
		let layers = difficulty || 1;

		return new Promise(function(resolve, reject) {
			let resultId = nanoid();

			// TODO select image
			let image = gm('./assets/' + sourceImage + '.png');
			image.noProfile();
			image.autoOrient();

			// HACK TODO remove
			let filters = [];
			for (let i = 0; i < layers; i++) {
				let filterName = this.chooseFilter(filters, difficulty);

				// prevent duplicates
				filters.push(filterName);

				this.applyFilter(image, filterName);
			}

			image.write('./distorted/' + resultId + '.png', function (err) {
				if (err) {
					reject(err);
				} else {
					resolve(resultId);
				}
			});
		}.bind(this));
	},
	applyFilter: function (image, filter) {
		let handler = require('./filters/' + filter);

		handler.apply(image);
	},
	chooseFilter: function (prev, difficulty) {
		let pool = filterPools[difficulty];

		let filter = _.sample(pool);

		// retry until we have a unique -- hacky, but it should work
		if (prev.includes(filter)) {
			return this.chooseFilter(prev, difficulty);
		}

		return filter;
	}
};
