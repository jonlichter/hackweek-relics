// for vibrant/good looking colors
let randomColor = require('randomcolor');

module.exports = {
	apply: function (image) {
		let color = randomColor({
			format: 'rgb',
			luminosity: 'light'
		});
		let matched = color.match(/rgb\(([0-9]+), ([0-9]+), ([0-9]+)\)/);

		// image.colorize(matched[1], matched[2], matched[3]);
		image.colorize(matched.slice(1, 3).join(','));
	}
};
