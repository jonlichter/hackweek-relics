let _ = require('lodash');

module.exports = {
	apply: function (image) {
		let intensity = _.random(1, 20, true);
		
		image.charcoal(intensity);
	}
};
