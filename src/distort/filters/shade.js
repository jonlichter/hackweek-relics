let _ = require('lodash');

module.exports = {
	apply: function (image) {
		let azimuth = _.random(0, 360, true);
		let elevation = _.random(5, 25, true);

		image.shade(azimuth, elevation);
	}
};
