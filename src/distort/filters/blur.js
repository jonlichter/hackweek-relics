let _ = require('lodash');

module.exports = {
	apply: function (image) {
		let radius = _.random(60, 100, true);
		let sigma = _.random(4, 12);

		image.blur(radius, sigma);
	}
};
