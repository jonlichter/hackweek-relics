let _ = require('lodash');

module.exports = {
	apply: function (image) {
		let intensity = _.random(3, 10, true);

		image.spread(intensity);
	}
};
