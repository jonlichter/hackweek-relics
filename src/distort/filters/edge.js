let _ = require('lodash');

module.exports = {
	apply: function (image) {
		let intensity = _.random(2, 10, true);

		image.edge(intensity);
	}
};
