let _ = require('lodash');

module.exports = {
	apply: function (image) {
		let degrees = _.random(0, 360, true);
		let backgroundColor = '#36393E';

		image.rotate(backgroundColor, degrees);
	}
};
