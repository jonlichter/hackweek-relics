let _ = require('lodash');

module.exports = {
	apply: function (image) {
		let intensity;

		if (Math.random() > 0.5) {
			intensity = _.random(-200, -50, true);
		} else {
			intensity = _.random(25, 100, true);
		}

		// TODO use .region to change the center of the affected region
		image.swirl(intensity);
	}
};
