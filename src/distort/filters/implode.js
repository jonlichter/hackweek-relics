let _ = require('lodash');

module.exports = {
	apply: function (image) {
		let intensity;

		if (Math.random() < 0.5) {
			intensity = _.random(-3, -1, true);
		} else {
			intensity = _.random(0.1, 0.7, true);
		}

		image.implode(intensity);
	}
};
