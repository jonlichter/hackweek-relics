let _ = require('lodash');

module.exports = {
	apply: function (image) {
		let intensity = _.random(1000, 10000, false);

		image.cycle(intensity);
	}
};
