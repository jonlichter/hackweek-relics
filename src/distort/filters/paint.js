let _ = require('lodash');

module.exports = {
	apply: function (image) {
		let intensity = _.random(5, 15, true);

		image.paint(intensity);
	}
};
