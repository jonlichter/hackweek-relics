let rarities = [
	'Common',
	'Uncommon',
	'Rare',
	'Epic',
	'Mythic'
];
let raritySets = [
	'commons',
	'uncommons',
	'rares',
	'epics',
	'mythics',
];
let rarityShort = [
	'C',
	'U',
	'R',
	'E',
	'M'
];
let rarityColors = [
	null,
	0x3adec0, // balance green
	0xf67b63, // brilliance orange
	0x9c81f2, // bravery purple
	0x7289DA // blurple
];

let cards = [
	{
		id: 'test_card',
		type: 'card',
		name: 'Testing Card',
		description: 'A card for testing.',
		asset: 'cards/card_testing',
		rarity: 4,
		sets: [],
		hidden: true
	},
	// help center cards
	{
		id: 'discord_announcements',
		type: 'card',
		name: 'Announcements',
		description: '',
		asset: 'cards/card_discord_announcements',
		rarity: 0,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_billing',
		type: 'card',
		name: 'Billing',
		description: '',
		asset: 'cards/card_discord_billing',
		rarity: 0,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_chat',
		type: 'card',
		name: 'Chat',
		description: '',
		asset: 'cards/card_discord_chat',
		rarity: 1,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_faq',
		type: 'card',
		name: 'Help Center',
		description: '',
		asset: 'cards/card_discord_faq',
		rarity: 0,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_gamedev',
		type: 'card',
		name: 'Developer Help Center',
		description: '',
		asset: 'cards/card_discord_gamedev',
		rarity: 0,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_gamestore',
		type: 'card',
		name: 'Gamestore',
		description: '',
		asset: 'cards/card_discord_gamestore',
		rarity: 1,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_interface',
		type: 'card',
		name: 'Interface',
		description: '',
		asset: 'cards/card_discord_interface',
		rarity: 1,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_programs',
		type: 'card',
		name: 'Community Programs',
		description: 'Hypesquad!',
		asset: 'cards/card_discord_programs',
		rarity: 2,
		sets: ['discord', 'hypesquad', 'random']
	},
	{
		id: 'discord_serversetup',
		type: 'card',
		name: 'Server Setup',
		description: '',
		asset: 'cards/card_discord_serversetup',
		rarity: 1,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_settings',
		type: 'card',
		name: 'Settings',
		description: '',
		asset: 'cards/card_discord_settings',
		rarity: 1,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_trustsafety',
		type: 'card',
		name: 'Trust & Safety',
		description: '',
		asset: 'cards/card_discord_trustsafety',
		rarity: 2,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_twitter',
		type: 'card',
		name: 'Discord On Twitter',
		description: '',
		asset: 'cards/card_discord_twitter',
		rarity: 0,
		sets: ['discord', 'random']
	},
	// hypesquad houses
	{
		id: 'discord_house_brilliance',
		type: 'card',
		name: 'House of Brilliance',
		description: 'The Hypesquad house of Brilliance.',
		asset: 'cards/card_discord_brilliance',
		rarity: 3,
		sets: ['discord', 'hypesquad']
	},
	{
		id: 'discord_house_bravery',
		type: 'card',
		name: 'House of Bravery',
		description: 'The Hypesquad house of Bravery.',
		asset: 'cards/card_discord_bravery',
		rarity: 3,
		sets: ['discord', 'hypesquad']
	},
	{
		id: 'discord_house_balance',
		type: 'card',
		name: 'House of Balance',
		description: 'The Hypesquad house of Balance.',
		asset: 'cards/card_discord_balance',
		rarity: 3,
		sets: ['discord', 'hypesquad']
	},

	{
		id: 'discord_streaming',
		type: 'card',
		name: 'Streaming',
		description: '',
		asset: 'cards/card_discord_streaming',
		rarity: 0,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_captcha',
		type: 'card',
		name: 'Captcha',
		description: '✓ I\'m not a robot!',
		asset: 'cards/card_discord_captcha',
		rarity: 0,
		sets: ['discord', 'random']
	},

	{
		id: 'discord_nitroclassicwumpus',
		type: 'card',
		name: 'Nitro Classic Wumpus',
		description: '',
		asset: 'cards/card_discord_nitroclassicwumpus',
		rarity: 1,
		sets: ['discord', 'nitro', 'wumpus', 'random']
	},
	{
		id: 'discord_nitrowumpus',
		type: 'card',
		name: 'Nitro Wumpus',
		description: '',
		asset: 'cards/card_discord_nitrowumpus',
		rarity: 2,
		sets: ['discord', 'nitro', 'wumpus', 'random']
	},

	{
		id: 'discord_ragingdemon',
		type: 'card',
		name: 'Raging Demon',
		description: 'A Discord easter egg. Open the keybinds screen with Ctrl + / and press `H` `H` `right arrow` `N` `K`!',
		asset: 'cards/card_discord_ragingdemon',
		rarity: 3,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_empathybanana',
		type: 'card',
		name: 'Empathy Banana',
		description: 'Empathy Banana is always here for you.',
		asset: 'cards/card_discord_empathybanana',
		rarity: 2,
		sets: ['discord', 'random']
	},
	{
		id: 'discord_nitroboost',
		type: 'card',
		name: 'Nitro Boost',
		description: '',
		asset: 'cards/card_discord_nitroboost',
		rarity: 0,
		sets: ['discord', 'nitro', 'random']
	},

	// dhw cards
	{
		id: 'dhw_souvenir',
		type: 'card',
		name: 'Discord Hack Week',
		description: 'This commemorative card celebrates Discord Hack Week 2019.',
		asset: 'cards/card_dhw_souvenir',
		rarity: 4,
		sets: ['discord', 'hackweek']
	},
	{
		id: 'dhw_wumpus',
		type: 'card',
		name: 'Wumpus\'s Ship',
		description: 'Wumpus\'s ship from Discord Hack Week 2019.',
		asset: 'cards/card_discord_wumpusship',
		rarity: 1,
		sets: ['discord', 'hackweek', 'wumpus', 'random']
	}/*,
	{
		id: 'dhw_badge',
		type: 'badge',
		name: 'Hack Week Badge',
		description: 'A badge from Discord Hack Week 2019.',
		asset: 'card_test', // TODO new asset from hack_badge_white
		rarity: 1,
		sets: ['discord', 'hackweek']
	}*/
];

// auto-sort into sets by rarity
cards.forEach(function (card) {
	if (!card.hidden) {
		card.sets.push(raritySets[card.rarity]);
	}
});

let bySet = {};

// process by-set listing
cards.forEach(function (card) {
	if (!card.hidden) {
		card.sets.push('all');
		card.sets.push('cg');
	}

	card.sets.forEach(function (set) {
		if (!bySet[set]) {
			bySet[set] = [];
		}

		bySet[set].push(card);
	});
});

// build setSizes
let setSizes = {};
Object.keys(bySet).forEach(function (setName) {
	setSizes[setName] = bySet[setName].length;
});

let byId = {};

// process by-id listing
cards.forEach(function (card) {
	byId[card.id] = card;
});

module.exports = {
	// don't show completion for these sets, they are used in the backend code
	secretSets: ['random', 'cg'],
	sets: Object.keys(bySet),
	setSizes: setSizes,

	all: cards,
	bySet: bySet,
	byId: byId,

	rarities: rarities,
	raritySets: raritySets,
	rarityShort: rarityShort,
	rarityColors: rarityColors
};
