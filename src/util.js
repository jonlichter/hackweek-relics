let Discord = require('discord.js');

module.exports = {
	isDev: function (member) {
		return ['286737962769580032', '220625168228286464'].includes(member.id);
	},

	parseMention: function (mention) {
		mention = mention.slice(2, -1);
		if (mention.startsWith('!')) {
			mention = mention.slice(1);
		}
		return mention;
	},

	embedPositive: async function (channel, title, desc) {
		await channel.send({
			embed: {
				author: {
					name: _bot.bot.user.username,
					icon_url: _bot.bot.user.avatarURL
				},
				color: constants.EMBED_POSITIVE,
				title: title,
				description: desc,
				timestamp: new Date(),
				footer: {
					icon_url: _bot.bot.user.avatarURL,
					text:'Discord Relics - A Hackweek Project'
				}
			}
		});
	},

	embedNegative: async function (channel, title, desc) {
		await channel.send({
			embed: {
				author: {
					name: _bot.bot.user.username,
					icon_url: _bot.bot.user.avatarURL
				},
				color: constants.EMBED_NEGATIVE,
				title: title,
				description: desc,
				timestamp: new Date(),
				footer: {
					icon_url: _bot.bot.user.avatarURL,
					text:'Discord Relics - A Hackweek Project'
				}
			}
		});
	},

	embedInfo: async function (channel, title, desc) {
		await channel.send({
			embed: {
				author: {
					name: _bot.bot.user.username,
					icon_url: _bot.bot.user.avatarURL
				},
				color: constants.EMBED_INFO,
				title: title,
				description: desc,
				timestamp: new Date(),
				footer: {
					icon_url: _bot.bot.user.avatarURL,
					text:'Discord Relics - A Hackweek Project'
				}
			}
		});
	},

	// probably broken unless used in the support server
	log: function (guild, action, title1, field1, title2, field2, title3, field3) {
		let embedLog = new Discord.RichEmbed()
			.setColor(constants.EMBED_INFO)
			.setAuthor(action, '', '')
			.addField(title1, field1)
			.addField(title2, field2)
			.addField(title3, field3);

		guild.channels.get('592809471303221297').send(embedLog);
	}
};
