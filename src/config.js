// Environment variables are set in the Heroku Dashboard so that when we host, we can keep the token secret.

let config = {
	alias: {
		//'p': 'ping'
		'invite': 'info',
		'uptime': 'info',

		'stats': 'profile',

		'inv': 'inventory',

		'start': 'intro',
		'tutorial': 'intro',
		'guide': 'intro',

		'progress': 'completion',
		'comp': 'completion',

		'cg': 'cardguess',

		'i': 'item',
		'c': 'claim',
		'p': 'pack'
	},
	prefix: 'r.'
};

let source;
if (process.env.IS_HEROKU) {
	source = process.env;

} else {
	try {
		let file = require('./../secret.js');
		source = file;
	} catch (e) {
		console.log(e);
		console.error('Couldn\'t load necessary configs from environment variables or from secret.js');
		process.exit(1);
	}
}

config.token = source.DISCORD_TOKEN;
config.id = source.DISCORD_ID;
config.rethinkdb_ip = source.RETHINKDB_IP;
config.rethinkdb_db = source.RETHINKDB_DB;
config.dbl_token = source.DBL_TOKEN;

module.exports = config;
