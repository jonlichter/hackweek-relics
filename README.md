# Discord Relics: A Hackweek 2019 Bot
<img src="assets/logo/relics_hackweek_badge.png" alt="Discord Hackweek badge" width="128" height="128" align="right"/>

![](https://img.shields.io/static/v1.svg?label=Version&message=Public%20Beta&color=BLUE) ![](https://img.shields.io/static/v1.svg?label=Hackweek%20Category&message=Entertainment&color=GREEN) ![](https://img.shields.io/static/v1.svg?label=Collaborators&message=3&color=YELLOW)

Discord Relics is a card-collecting entertainment bot created during Discord Hack Week 2019 by notme#1560, RJain#8976, and avistah#2485. Invite the bot and use `r.intro` for a text-based guide to the bot.

* [Bot Invite](https://discordapp.com/oauth2/authorize?client_id=592804506908098572&scope=bot&permissions=35840)
* [Support Server](https://discord.gg/p89esuZ)
* On [Discord Bot List](https://discordbots.org/bot/592804506908098572)
* On [Discord Server List](https://discordbots.org/servers/592803740914942032)
* [Trello](https://trello.com/b/4Lg4voKZ/discord-relics-a-hackweek-2019-bot)

[![Discord Bots](https://discordbots.org/api/widget/592804506908098572.svg)](https://discordbots.org/bot/592804506908098572)

## Usage
`r.intro` for a text-based tutorial.

`r.profile` to view your stats.

`r.daily` to claim some daily currency.

For a limited time during Hackweek, `r.promo hackweek2019` to claim an exclusive Hackweek card.

`r.shop view` and `r.shop buy <offer name>` to buy cards and packs.

`r.pack list` and `r.pack open <pack name>` to open packs and get cards.

`r.inventory` to see your cards and `r.completion` to see completion progress on different sets of cards.

`r.cardguess start` starts a minigame where you have to guess the distorted card and win some currency.

`r.item <item name>` to view a card and its information.

`r.help` for a list of commands and their usages.

`r.claim` to claim random item drops.

`r.config itemdrop true/false` to disable random item drops for the current guild (user needs `MANAGE_GUILD` permission).

`r.pay` and `r.gift` to send currency and items to other users.

`r.info` to get the bot's support server, invite, uptime, and other information.

`r.vote` to get the Discord Bot List voting link.

`r.ping` to check latency/response time.

## Dependencies
If you're trying to run the bot for yourself, you will need to install the NPM packages from `package.json` (`npm i`) and install GraphicsMagick (`sudo apt-get install graphicsmagick` worked for me, but you may need to install it differently depending on your OS and setup) for image manipulation.

`secret.js` will need a few configuration variables.
The file should look like this:

```js
module.exports = {
	DISCORD_TOKEN: '',
	DISCORD_ID: '',
	DBL_TOKEN: '',
	RETHINKDB_IP: '',
	RETHINKDB_DB: ''
};
```

The Discord token is used to log in the bot and the ID is used to generate invite links and mention the bot (might not be necessary).
`DBL_TOKEN` is for reporting stats to Discord bots list. If it is left blank the bot just won't report stats so it isn't needed.

The bot needs a running RethinkDB instance for data persistence.
The IP can just be `localhost` (it is configurable so I can run the production bot and database on one machine and the development bot on my computer but connect to the same RethinkDB instance).
`RETHINKDB_DB` is the name of the RethinkDB database used (for example, `discord_relics` and `discord_relics_dev`).
The bot will automatically create the database and tables it needs within the RethinkDB instance if they don't exist.

Of course, if you want to just test the bot without installing everything you can: [invite](https://discord.gg/p89esuZ) and [support server](https://discordapp.com/oauth2/authorize?client_id=592804506908098572&scope=bot&permissions=35840).

## Credits
Bot profile picture is [Relic blade icon by Lorc under CC BY 3.0](https://game-icons.net/1x1/lorc/relic-blade.html) from game-icons.net.

# Process
## Developers
We had a team of three people, known on Discord as notme#1560, RJain#8976, and avistah#2485.
* notme#1560 was our lead developer and had the most experience on the team working with Node, Discord.JS, and RethinkDB.
* RJain#8976 was the project manager; he did some programming but mainly organized the team and coordinated the work.
* avistah#2485 was travelling but was able to help us organize our ideas and balance some of the values.

## The Journey
Our team has known each other for the past three years and has even done a few programming and Discord bot projects together before.
The Discord Hackweek was a LOT of fun: all of us love programming and expressing our creativity and spend a lot of time on Discord.
With a project like this one, we believe there is never really a true end.
That is why we are not stopping after the Hackweek ends! We would like to continue hosting our bot and bringing communities together through entertainment, fun, and games.

## Development
*Development writeup by notme#1560, feel free to DM and ask about the bot.*

Everyone who has made a game or Discord bot or any kind of personal project without an incentive or requirement to finish (employment, education, or monetary necessity) knows the feeling of abandoning one project and starting another.
At least, I do, and `~/projects` has 78 half-finished projects to support that claim.
Discord Hack Week was a good opportunity for us to pick an achievable, limited scope for the project and see it all the way through from design and production to release and support.

Since I have the most experience in it, we decided to use Node.JS (and NPM for easy-but-inefficient dependency installation) and my other previous bot projects have provided me with experience in Discord.JS as well, so we used the library to interface with Discord.

RethinkDB is a simple database which stores records in JSON with its own query language, ReQL, which makes it very easy for anyone to persist data. It has libraries for Node (as well as Python and Ruby if you are using those) and we picked it over Postgres (and an ORM) and Firebase.

Lodash/Underscore is a very, very helpful library with a lot of small utility functions. Mainly, we use `chunk`, `cloneDeep`, `random`, and `sample` to avoid writing out lengthy, repeated bits of code.
`string-similarity` is used for fuzzy searching of item names in CardGuess and item drops as well as in other commands searching for item information.
`randomcolor` generates random colors that aren't ugly browns and grays.
`pretty-ms` converts milliseconds to a human-readable format with seconds, minutes, hours, days, and so on.
`nanoid` generates unique IDs which (theoretically) shouldn't collide.
`node-os-utils` is used in the `admin` command to view CPU and memory usage, etc.
`dblapi.js` posts server count to the Discord Bot List [here](https://discordbots.org/bot/592804506908098572)

The bot (during Hack Week) was hosted on a spare laptop. RethinkDB has an autostart service and I used PM2 to automatically start the sharding manager when the laptop restarts. The sharding manager automatically restarts shards and a commnad allows us to restart all shards remotely through Discord, so the only interaction with the server is when updating which is done with a `git pull`.

The token and other private variables are kept in a `secret.js` file which is ignored by git. Each development installation has its own secret file so each developer can run their own instance of the bot and push changes to the master version. The production tokens are sent to the server over ssh/scp or they can be read from environment variables if we were to host on Heroku.

The Shard Manager is very simple and automatically starts the needed number of shards. It also listens to the shards in case a developer requests a shard or bot restart. Each shard connects to the database separately. The actual bot is fairly standard.

For CardGuess and item drops, item images are distorted and the players have to guess what the original image was.
Distortion is done using `gm`, Node bindings for GraphicsMagick. There are a handful of filters and the bot chooses 2–3 of these and applies them to the source image. Each filter randomizes its effects (intensity, color, etc.) in order to make the results less predictable.

I made the card images in Gimp.
