let Discord = require('discord.js');

let manager = new Discord.ShardingManager('./shard.js', {
	totalShards: 'auto',
	shardList: 'auto',
	mode: 'process',
	respawn: true,
	token: process.env.DISCORD_TOKEN || require('./secret').DISCORD_TOKEN
});

let shards = [];

manager.spawn();
manager.on('launch', function (shard) {
	console.log('[MANAGER] Launched shard ' + shard.id);

	shards.push(shard);

	// shard.send(

	shard.on('message', function (message) {
		if (message.action == 'restart') {
			console.log('[MANAGER] Restarting all shards (requested by shard ' + shard.id + ')');
			manager.respawnAll();
		} else if (message.action == 'restartShard') {
			console.log('[MANAGER] Restarting shard ' + shard.id);
			shard.respawn();
		} else if (message.action == 'shutdown') {
			console.log('[MANAGER] Killing all shards and exiting (requested by shard ' + shard.id + ')');
			for (let i = 0; i < shards.length; i++) {
				shards[i].kill();
			}
			process.exit(0);
		}
	});
});
