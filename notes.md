# Development Notes

## Border Instructions
600 x 840
Select All
Shrink 25
Invert
Rounded Rectangle (5%, Convex)
[Delete] -- make white, not transparent
Add Alpha Channel
Select All
Rounded Rectangle (5%, Convex)
Invert
[Delete]

Images ~400x400 in the center vertically and horizontally

Uni Sans Heavy
Size 72
Center justify
Dynamic box

Align tool to center text on page
Move to bottom border of box is on the blurple border

Exports:
no interlacing
save background
dont save gamma
dont save layer offset
save resolution
dont save creation time
dont save comment
save color values from transparent pixels
compression 9 (should be lossless though)

## Asset URLs
https://discordapp.com/assets/8c998f8fb62016fcfb4901e424ff378b.svg miner wumpus
https://discordapp.com/assets/dd9f22c9e2365062db9d5f75c870e5d7.svg wumpus w/ tv (find friends)
x https://discordapp.com/assets/7558b67985f4035ed2b2cf9e3072d81f.svg nitro wumpus
x https://discordapp.com/assets/be14b7a8e0090fbb48135450ff17a62f.svg nitro classic wumpus

https://discordapp.com/assets/72e1fd085e240aecf005e642e655c0f4.svg arcade machine
https://discordapp.com/assets/e9aaf1824f17126a7992e5ad98752389.svg connections
https://discordapp.com/assets/cdea41ede63f61153e4a3c0531fa3873.svg 2fa
x https://discordapp.com/assets/616e078b351d0df460971441949c53a3.svg streaming

https://discordapp.com/assets/1cbd08c76f8af6dddce02c5138971129.png example pfp

https://discordapp.com/assets/fd91131ea693096d6be5e8aa99d18f9e.jpg login background
https://discordapp.com/assets/3bd0d83cb24ccf918b757ab5c18dc564.svg party
https://discordapp.com/assets/213acc14046cd858588e10b2e35ab7da.svg alien stand

x https://discordapp.com/assets/0f4d1ff76624bb45a3fee4189279ee92.svg captcha key

https://discordapp.com/assets/0c78ea6377b186e6675fed771165efc0.png welcome banner

https://i.ytimg.com/vi/ZyX7U78keu0/maxresdefault.jpg?width=400&height=226 nitro boost thumbnail

https://discordapp.com/assets/abaa70ae2be9f361759c60b2ee85ff12.png hack week changelog image

Houses
x https://discordapp.com/assets/8c59bddd746488fe8acf23d8d36c1a60.svg brilliance white f67b63
https://discordapp.com/assets/0f1eadc1ec3616e0b1c9ce515ee5683a.svg brilliance black
x https://discordapp.com/assets/a359371246d6353de6bc32715e092eba.svg bravery white 9c81f2
https://discordapp.com/assets/59419b0683ef50f5e8ecd8748ecbcfeb.svg bravery black
x https://discordapp.com/assets/07ed671b6c823a6cae5be39860ac8bf0.svg balance white 3adec0
https://discordapp.com/assets/ac89ab7dd6f700d02b0ac958b98a3309.svg balance black

Home
https://discordapp.com/assets/0b5a0339571e72656eea93eb55d73eae.svg potion
https://discordapp.com/assets/eb301f28da3199edbd3ef19690d61674.svg bomb
https://discordapp.com/assets/9e05338bd66e0985fceb83317cb94b9c.svg coin
https://discordapp.com/assets/215346366a9a7d50924fc245ddb048d2.svg disk
https://discordapp.com/assets/15149ecb9d5cd8faa24e1bbf45d70e5b.svg shield?
https://discordapp.com/assets/81d74b2ebb053fbccee41865a47d48c3.svg mystery box
Home Platforms
https://discordapp.com/assets/0d82411c439e3558f8b2f6fb12eccbc1.svg desktop
https://discordapp.com/assets/7edaed9d86e1b5dd9d4c98484372222b.svg laptop
https://discordapp.com/assets/69db64955960eb333f5ff831cc1c0294.svg laptop headphones
https://discordapp.com/assets/5a31f41848bf3ba1817a092ac28c623d.svg mobile
https://discordapp.com/assets/82fa4f388cfc9cf47a6972ae39ae90de.svg mobile apple
https://discordapp.com/assets/c4bae281354a2b4e2db85415955e0994.svg controller

Job Benefits
https://discordapp.com/assets/c76afa4fb27661cbae7efb5a0b02f09c.svg health
https://discordapp.com/assets/6dd04abdb247c75c1c558c6c63067bc7.svg parental leave
https://discordapp.com/assets/f1d8db09355b3a5418ca4c543f36b297.svg leisure time
https://discordapp.com/assets/2afe1ef6f973e4410f26611085779b1b.svg commute
https://discordapp.com/assets/07ade023499544549a1645d8d2690338.svg food
https://discordapp.com/assets/9921e468bcb3cab444dbcdcd9571b7b8.svg fitness
https://discordapp.com/assets/85344d6cc6e19b42fb29182079f7efdc.svg insurance
https://discordapp.com/assets/fef1118bfcf3a5ad6470d694c8db4bb7.svg hq
https://discordapp.com/assets/94be0a766a29200f7012b7f3f94dd833.svg relocation

User Verification
https://discordapp.com/assets/c290235278e128e94ae5ac37e58c5cbb.svg castle under attack
https://discordapp.com/assets/cefc9c14adce616059f519c581331b32.svg email verify
https://discordapp.com/assets/ca452f5271ebcc7132db59f60a2a9cfe.svg phone verify

Server Verification
https://discordapp.com/assets/28ed36711a92364163bb6dd969e706b5.svg tick
https://discordapp.com/assets/1d35d6bfbe45dff49e7191bdec2f9321.svg vanity
https://discordapp.com/assets/8db9b9007eb178203eb1d043b6247fd6.svg servers

Partner Perks
https://discordapp.com/assets/f2c5395aac0bfa256dc6900a48d32ae9.svg nitro
https://discordapp.com/assets/54a18583409e209c7045d3f26dc8d193.svg vanity
https://discordapp.com/assets/2fa182a4225ca0215c64526a6fd54b12.svg vip servers
https://discordapp.com/assets/2a2d92a2f55ca84ce5775c6944fe7363.svg partner badges
https://discordapp.com/assets/22d12373a67ca5ba60c9b89869ebe07d.svg hoodie
https://discordapp.com/assets/597bbd9bfa76df4da7c95110cc454f29.svg rewards

HypeSquad
https://discordapp.com/assets/19654c38399b0e75c351d6fc960fe0ca.svg apply
https://discordapp.com/assets/84d4951c6eb1f90d3cc51c8a696cc1a0.png swag
https://discordapp.com/assets/1fd30dc5922029b453afe4c78e10873b.svg share the news

HypeSquad mini icons
https://discordapp.com/assets/104aed05aa971c3f6ec263c6d0314927.png badge
https://discordapp.com/assets/2402224b8957788a59a1bed474d77d6e.png hoodie
https://discordapp.com/assets/2e972c97c3483bf0bee8fc011a77044d.png server
https://discordapp.com/assets/5d0876ba7f8b081321655a7fc5c60aa2.png rewards

Help Center
x https://theme.zdassets.com/theme_assets/678183/1bd76d1fd516f8dacd3c7422a8ceb929ef2502d9.svg announcements
x https://theme.zdassets.com/theme_assets/678183/8c6c6469bdca01e05d2870e6d373c1d80e290255.svg interface
x https://theme.zdassets.com/theme_assets/678183/3da6524afd9979b44c5f6ceaf19e812c204ad74e.svg settings
x https://theme.zdassets.com/theme_assets/678183/35f736c34c81d953054d0c1e202f71c8094f8fbf.svg chat
x https://theme.zdassets.com/theme_assets/678183/33f0bd0c3a2c58763cb0404a18bb38da48fc289d.svg server setup
x https://theme.zdassets.com/theme_assets/678183/dd93acfbff40b1ea53f2b4e8fa51ac142486bd13.svg programs
x https://theme.zdassets.com/theme_assets/678183/d7415f17818f8caabf5d98099171b227201a3e41.svg game store
x https://theme.zdassets.com/theme_assets/678183/e5bd3c63530b0a8e55b406528f2fd9d61b8bd923.svg billing
x https://theme.zdassets.com/theme_assets/678183/6f28e83ed890200c649526472dec1e8f593fe1dd.svg trust and safety
x https://theme.zdassets.com/theme_assets/678183/1883060a5132c470528a20c1cfe42976e7c167ca.svg faqs
x https://theme.zdassets.com/theme_assets/678183/66a7b3885ab7dccb734dd94b9ed3408fc410d30c.svg gamedevs
x https://theme.zdassets.com/theme_assets/678183/ceb4c1d378277188ab383a50029b43e5d3b1581b.svg twitter

https://discordapp.com/assets/e5c8ffdcf564b768a95069c19629547c.svg bug bounty
