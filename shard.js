// https://www.schemecolor.com/pastel-rainbow.php
global.constants = {
	BLURPLE: 0x7289DA,
	BRILLIANCE: 0xf67b63,
	BRAVERY: 0x9c81f2,
	BALANCE: 0x3adec0,
	EMBED_INFO: 0x7289DA,
	EMBED_POSITIVE: 0x60bf60, // 0x9ee09e,
	EMBED_NEGATIVE: 0xff6663,
	EMBED_WARN: 0xfeb144
};

// let distort = require('./src/distort/engine');
let database = require('./src/database');
let bot = require('./src/bot');
let util = require('./src/util');

// hacky
global._bot = bot;
global._database = database;
global._util = util;

async function start () {
	// await distort.clean();

	bot.init();

	await database.init();
}

start();

// TODO sharding??
